/* 
    Created on : 4-nov-2019
    Author     : Jan-Dirk van Dingenen
                 boardwebgames.com
*/

$(document).ready(function(){
    const HEART = 'heart';

    const BODY = 'body';
    const COIN = 'coin';
    const FOOD = 'food';
    const STAR = 'star';

    const MIND = 'mind';
    const ENERGY = 'energy';
    const FEAR = 'fear';
    const WISDOM = 'wisdom';

    const PRESENT = 'present';
    const PAST = 'past';
    const FUTURE = 'future';
    const POSSIBLE = 'possible';

    const ITEM = 'item';

    var decks = {};
    var items;
    var eila;
    var game;
    var intro;
    var sound;
    var tutorial;
    var storyBook;
    
    class StoryBook {
        constructor() {
            this.stories = [];
            for(let s=0; s<= 6; s++) {
                this.stories.push(new Story(s));
            }
        }
        
        startStory(difficulty,won,after) {
            if(!after) {
                if(difficulty !== 'tutorial') {
                    this.playStory(2,true);
                    return true;
                }
                return false;
            }
            else {
                if(difficulty === 'tutorial') {
                    if(won) {
                        this.playStory(0);
                        return true;
                    }
                    else {
                        this.playStory(1);
                        return true;
                    }
                }
                else {
                    if(!won) {
                        this.playStory(5);
                        return true;
                    }
                    else {
                        if(difficulty === 'advanced') {      
                            this.playStory(3);
                        }   
                        else {
                            this.playStory(4);
                        }
                    }
                }
            }
        }
        
        playStory(storyNum,prequel) {
            let thisStory = this.stories[storyNum];
            thisStory.play(prequel);
        }
    }
    
    class Story {
        constructor(storyNumber) {
            this.storyLine = [];
            if(storyNumber === 0) {
                // chapter 0 Win end
                this.storyLine = ['story001','story002','story003','story004','story005','story006'];
            }            
                        
            if(storyNumber === 1) {
                // chapter 0 Lose  end
                this.storyLine = ['story001','story207','story208','story210'];
            }
            
            if(storyNumber === 2) {
                // chapter 1 prologue
                this.storyLine = ['story101','story102','story103','story104','story105'];
            }
            
            if(storyNumber === 3) {
                // chapter 1 advanced : full end
                this.storyLine = ['story201','story202','story203','story204','story205','story206','story207','story208','story209','story210','story211','story212','story213','story214'];
            }
            
            if(storyNumber === 4) {
                // chapter 1 normal : short end
                this.storyLine = ['story201','story212','story213','story214'];
            }
            
            if(storyNumber === 5) {
                // chapter 1 : Failed end
                this.storyLine = ['story201','story207','story208','story210'];
            }
        }
        
        play(prequel) {
            $('#cardHandle').hide();
            $('.inventorySlot').hide();
            $('#goalCard').hide();
            $('#formulas').hide();
            $('#storyBook').html('');
            var self = this;
            for(let [index,page] of this.storyLine.entries()) {
                let newPage = $('<img/>').addClass('storyPage').attr('src','img/'+page+'.jpg'); 
                if(index === 0) {
                    newPage.addClass('binderPage');
                }
                newPage.on('click',function(event){
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).addClass('removePage');
                    if(index >= self.storyLine.length -1) {
                        self.done(prequel);
                    }
                });                
                $('#storyBook').prepend(newPage);
            }            
            
            $('#storyBook').fadeIn();
        }
        
        done(prequel) {
            $('#storyBook').fadeOut();            
            $('.inventorySlot').fadeIn();
            $('#goalCard').fadeIn();
            $('#formulas').fadeIn();
            if(prequel) {
                $('#cardHandle').fadeIn();
            }           
            else {
                $('#goalHandle').trigger('click');
            }
        }
    }
    
    class Intro {
        constructor() {
            this.preloads = [];    
            this.preloadChapter = [];
            this.baseUnit = 500;
            this.resizeTimeout;
            intro = this;
            
            window.onresize = function() {
                clearTimeout(intro.resizeTimeout);
                intro.resizeTimeout = setTimeout(function(){
                    intro.resize();
                },250);
            };
            
            this.resize();
            this.initSound();
            
            $('.gameOption_tutorial').on('click',function(){
               sound.play('click');
               intro.startGame('tutorial');
            });
            $('.gameOption_normal').on('click',function(){
               sound.play('click');
               intro.startGame('normal');
            });
            $('.gameOption_advanced').on('click',function(){
               sound.play('click');
               intro.startGame('advanced');
            });
            
            setTimeout(function(){
                intro.preLoadImages();
            },100);
        }

        resize() {
            let screenWidth = $(window).width();
            let screenHeight = $(window).height();
            if(screenWidth > screenHeight) {
                $('body').removeClass('verticalLayout');
                $('body').addClass('horizontalLayout');
            }
            else {
                $('body').addClass('verticalLayout');
                $('body').removeClass('horizontalLayout');
            }
            if(screenWidth*1.58 > screenHeight) {
                this.baseUnit = screenHeight/1.58;
            }
            else {
                this.baseUnit = screenWidth;        
            }

            this.baseUnit = Math.floor(this.baseUnit);
            $("body").get(0).style.setProperty('--baseUnit', this.baseUnit+'px');  
        }
        
        preLoadImages() {
            this.preLoadImage('img/resources.png'); 
            this.preLoadImage('img/cardback.jpg'); 
            this.preLoadImage('img/logoGlow.png'); 
            this.preLoadImage('img/mindFormula.png'); 
            this.preLoadImage('img/bodyFormula.png'); 
            this.preLoadImage('img/button.png'); 
            this.preLoadImage('img/popup.jpg'); 
            this.preLoadImage('img/arrow.png'); 
            this.preLoadImage('img/markers.png');
            this.preLoadImage('img/items.png');
            this.preLoadImage('img/001.jpg');
            this.preLoadImage('img/002.jpg');
            this.preLoadImage('img/003.jpg');         
            this.preLoadImage('img/101.jpg');
            this.preLoadImage('img/102.jpg');
            this.preLoadImage('img/103.jpg');
            this.preLoadImage('img/goal0.jpg');
            this.preLoadImage('img/goal1a.jpg');
            this.preLoadImage('img/goal1b.jpg');
            Promise.all(this.preloads)
                .then(results => {			
                    $('#loading').remove();
                    $('.box').removeClass('rotateBox');
                    setTimeout(function(){
                        $('.box-outer').addClass('turnBox');
                    },10);
                    setTimeout(function(){
                        $('#gameOptions').fadeIn();
                    },3000);
                    this.preloads = [];
                })
                .catch(err => console.error(err));
        };

        preLoadImage(src,target) {
            var newPromise =  new Promise((resolve, reject) => {
                    const img = new Image();
                    img.addEventListener("load", () => resolve());
                    img.addEventListener("error", err => reject(err));
                    img.src = src;
            });
            this.preloads.push(newPromise);
            return newPromise;
        };
        
        preloadChapter0() {           
            this.preLoadImage('img/004.jpg'); 
            this.preLoadImage('img/005.jpg');
            this.preLoadImage('img/006.jpg');
            this.preLoadImage('img/007.jpg');
            this.preLoadImage('img/008.jpg');
            this.preLoadImage('img/009.jpg');
            this.preLoadImage('img/010.jpg');
            this.preLoadImage('img/011.jpg');
            Promise.all(this.preloads).then(results => {			
                this.preloads = [];
            })
            .catch(err => console.error(err));
        }
        
        preloadChapter1() {      
            this.preLoadImage('img/104.jpg');            
            this.preLoadImage('img/105.jpg');
            this.preLoadImage('img/106.jpg');
            this.preLoadImage('img/107.jpg');
            this.preLoadImage('img/108.jpg');
            this.preLoadImage('img/109.jpg');
            this.preLoadImage('img/110.jpg');
            this.preLoadImage('img/111.jpg');
            this.preLoadImage('img/112.jpg');
            this.preLoadImage('img/113.jpg');
            this.preLoadImage('img/114.jpg');
            this.preLoadImage('img/115.jpg');
            this.preLoadImage('img/116.jpg');
            this.preLoadImage('img/117.jpg');
            this.preLoadImage('img/118.jpg');
            this.preLoadImage('img/119.jpg');
            this.preLoadImage('img/H01.jpg'); 
            this.preLoadImage('img/H02.jpg'); 
            this.preLoadImage('img/H03.jpg'); 
            this.preLoadImage('img/toolbox.jpg'); 
            this.preLoadImage('img/rope.jpg'); 
            this.preLoadImage('img/magnifier.jpg'); 
            this.preLoadImage('img/pendant.jpg'); 
            this.preLoadImage('img/120.jpg');
            this.preLoadImage('img/121.jpg');
            this.preLoadImage('img/122.jpg');
            this.preLoadImage('img/123.jpg');
            this.preLoadImage('img/124.jpg');
            this.preLoadImage('img/125.jpg');
            this.preLoadImage('img/126.jpg');
            this.preLoadImage('img/127.jpg'); 
            this.preLoadImage('img/128.jpg'); 
            Promise.all(this.preloads).then(results => {			
                this.preloads = [];
            })
            .catch(err => console.error(err));
        }
        
        startGame(difficulty) {
            $('#intro').fadeOut(function(){
                $('#intro').remove();
            });
            new Game(difficulty);  
            init(game.difficulty);
            game.initEvents();
            $('#wrapper').fadeIn();
            storyBook.startStory(difficulty,null,false);
        }
        
        initSound() {
            sound = new Howl({
                src: ['eila.mp3'], 
                sprite: {
                    card: [0, 450],    
                    click: [550, 100]
                }
            });
            Howler.volume(0.5); 
        }
    }

    class Game {
        constructor(difficulty) {
            game = this;                        
            this.difficulty = difficulty;
            this.goal;
            this.currentCard;
            this.resolveDirection;
            this.day = 1;
            this.dayEnd = false;
            this.dayFulfillment = 0;
            this.gameEnded = false;
            this.won = false;
            this.history = [];
            this.ended = false;
            
            this.overFlowResourceList = [];

            intro.resize();
            if(difficulty === 'tutorial') {
                tutorial = new Tutorial();
            }

            this.addHistory('start');
        }

        setStartCard(startCard) {
            startCard.show();
            decks[PRESENT].removeCard(startCard.id);
        }

        getNextCard(initiator) {
            if(this.currentCard) {
                decks[PRESENT].removeCard(this.currentCard.id);
                decks[this.resolveDirection].addCard(this.currentCard);                
            }
            
            let nextCard = decks[PRESENT].getFirstCard();
            
            this.currentCard = null;
            if(nextCard) {                      
                this.switchCard(this.resolveDirection, nextCard);
            }
            else {
                this.switchCard(this.resolveDirection, null);
                setTimeout(function(){
                    game.endDay();
                },500);
            }
        }

        initEvents() {
            this.updateTopBar();

            $('#goalHandle').on('click',function(event){
                event.preventDefault();
                event.stopPropagation();
                sound.play('click');
                $('#goalCard').addClass('open');
                $('#goalHandle').fadeOut();
                $('.showItem').remove();
            });

            $('#goalCard').on('click',function(event){
                event.preventDefault();
                sound.play('click');
                if(!game.dayEnd) {
                    event.stopPropagation();
                    $('#goalCard').removeClass('open');
                    $('#goalHandle').fadeIn();
                }
            });

            $('#bodyFormula').on('click',function(event){
                event.preventDefault();
                game.formulaClicked(BODY);            
            });

            $('#mindFormula').on('click',function(event){
                event.preventDefault();
                sound.play('click');
                game.formulaClicked(MIND);
            });

            $(document).on('click','.inventorySlot.hasResource .resource:not(.resource_fear)', function(event) {
                event.preventDefault();                
                let res = $(this).attr('res');
                if(game.dayEnd) {
                    sound.play('click');
                    game.goal.scoreGoalResource(res);
                }
                else {
                    if($(this).hasClass('canRemove')) {
                        sound.play('click');
                        let confirmHTML = '<div>Are you sure you want to remove this <div class="resource resource_'+res +'"></div> ?</div><div class="chooseone chooseConfirm"><button class="buttonYes">Yes</button><button class="popupOk">No</button></div>';
                        game.messagePopup(confirmHTML, null, null, true);
                        $('.buttonYes').on('click',function(){      
                            event.preventDefault();
                            eila.removeResource(res.toLowerCase(), 1);
                            $('.popupOk:first').trigger('click');
                        });
                    }
                }
            });

            $(document).on('click','.cardOption', function(event) {
                event.preventDefault();
                sound.play('click');
                let opt = parseInt($(this).attr('opt'));
                let chosen = game.currentCard.choices[opt];
                chosen.execute();
            });

            $(document).on('click','.popupOk', function(event) {
                event.preventDefault();
                sound.play('click');
                $('#popup').fadeOut();
                game.removeUnclick();
            });

            $(document).on('click','.modalOk', function(event) {
                $(this).hide();
                event.preventDefault();
                sound.play('click');
                $('#modalPopup').fadeOut();
                game.removeUnclick();
                game.getNextCard('modalOK');
            });                                               
        }

        formulaClicked(type) {
            let newTrade;
            if(type === BODY) {
                newTrade = new Trade(FOOD,2,ENERGY,1);
            }
            if(type === MIND) {
                newTrade = new Trade(WISDOM,6,STAR,1);
            }
            let tradeCheck = newTrade.check();
            if(tradeCheck === true) {
                newTrade.execute();
            }  
            if(tradeCheck === null) {
                game.messagePopup('No empty slot to trade to.','Ok');
            }
        }

        switchCard(deckTarget, newCard) {
            $('.cardOption').remove();
            $('#card3D').addClass('flipBack');
            $('#tutorial').hide();
            sound.play('card');
            setTimeout(function(){
                $('#cardHandle').addClass('move_'+deckTarget);
                setTimeout(function(){
                    $('#cardHandle').removeClass('move_past move_future move_possible').addClass('move_present');  
                    $('#card3D').removeClass('flipBack'); 
                    if(newCard) {                        
                        newCard.show();
                        setTimeout(function(){
                            if(!game.gameEnded) {
                                $('#cardHandle').show();
                                $('#cardHandle').removeClass('move_present');
                                game.addHistory('reveal card '+newCard.id);
                                if(game.difficulty === 'tutorial') {
                                    tutorial.cardTutorial(newCard.id);
                                }
                            }
                        },500);
                    }
                },500);
            },500);
        }

        endDay() {
            this.goal.scoredToday = 0;
            this.dayEnd = true;
            this.dayFulfillment = 0;
            $('#game').addClass('dayEnd');
            $('#goalHandle').trigger('click');
            game.dayEndPopup(1);         
            for(let card of decks[FUTURE].cards) {
                decks[PRESENT].addCard(card);                
            }
            decks[FUTURE].empty();
            decks[PRESENT].shuffle();
            this.updateTopBar();
            game.goal.updateOverlay();
            this.addHistory('end day '+this.day);
            if(tutorial && this.day === 1) {
                tutorial.cardTutorial('day1');
            }
        }

        updateTopBar() {
            $('#day span').text(this.day);
            $('#presentCards span').text(decks[PRESENT].cards.length);
            $('#futureCards span').text(decks[FUTURE].cards.length);
        }

        messagePopup(text, buttonText, buttonClass, withoutButton) {
            if(!buttonClass) {
                buttonClass = 'popupOk';
            }
            $('#cardWrapper').addClass('unclickable');
            $('#popup .popupContent').html(text);
            if(!withoutButton) {
                $('#popup .popupContent').append($('<button/>').text(buttonText).addClass('popupButton '+buttonClass));
            }
            $('#popup').fadeIn();
        }

        tradePopup(trade) {
            $('#cardWrapper').addClass('unclickable');
            $('#modalPopup .popupContent').html('');

            let tradeType = $('<div/>').addClass('tradeType');
            tradeType.append($('<div/>').addClass('resource resource_'+trade.from));
            tradeType.append($('<div/>').addClass('arrow'));
            tradeType.append($('<div/>').addClass('resource resource_'+trade.to));                        

            $('#modalPopup .popupContent').append('<div>Click to trade</div>');
            $('#modalPopup .popupContent').append(tradeType);
            $('#modalPopup .popupContent').append($('<button/>').text('Done').addClass('popupButton modalOk'));

            $('#modalPopup .popupContent .tradeType').on('click',function(event){
                event.stopPropagation();
                event.preventDefault();
                sound.play('click');
                let newTrade = new Trade(trade.from,1,trade.to,1);
                let tradeCheck = newTrade.check(true);
                if(tradeCheck) {
                    newTrade.execute();
                }
                if(tradeCheck === null) {
                    game.messagePopup('No empty slot to trade to.','Ok');
                }
            });

            $('#modalPopup').fadeIn();
        }

        manualAddPopup(resources) {
            // show all the resources in popup, click them to add.
            $('#cardWrapper').addClass('unclickable');
            $('#modalPopup .popupContent').html('<div class="manualAddTitle">Click to add</div>');

            let closingPopup = false;
            let manualResources = $('<div/>').addClass('manualAdd');
            manualResources.append('');
            let count = 0;
            game.overFlowResourceList = [];
            for(let resource of resources) {
                for(let amount = 0; amount < resource.a; amount++) {                    
                    game.overFlowResourceList.push({"name":resource.n,"added":false});
                    manualResources.append($('<button/>').addClass('addManualResource addManualResource_'+count).html('<div class="resource resource_'+resource.n+'"></div>').data('resNum',count));
                    count++;
                }
            }

            $('#modalPopup .popupContent').append(manualResources);
            
            function allowRemovalWhenFull() {
                // also checks if modal can be closed
                $('.canRemove').removeClass('canRemove');
                let types = new Set([]);
                for(let res of game.overFlowResourceList) {
                    if(res.added === false) {
                        types.add(eila.resourceCheck(res.name).type);
                    }
                }
                
                if(game.overFlowResourceList.filter(r => r.added === false).length > 0) {
                    if(eila.maxBody - eila.bodySlots() === 0 && types.has(BODY)) {
                        $('#bodyInventory .inventorySlot .resource').addClass('canRemove');
                    }
                    else {
                        $('#bodyInventory .inventorySlot .resource').removeClass('canRemove');
                    }
                    if(eila.maxMind - eila.mindSlots() === 0 && types.has(MIND)) {
                        $('#mindInventory .inventorySlot .resource').addClass('canRemove');
                    }      
                    else {
                        $('#mindInventory .inventorySlot .resource').removeClass('canRemove');
                    }
                }
                else {
                    
                    eila.override = false;
                    $('#modalPopup').fadeOut();
                    game.removeUnclick();
                    if(!closingPopup) {
                        setTimeout(function(){
                            game.getNextCard('manualAddPopup');
                        },100);  
                    }
                    closingPopup = true;
                }
            }

            $(document).on('click','.addManualResource', function(event) {
                event.preventDefault();
                sound.play('click');
                let resNum = $(this).data('resNum');
                let res = game.overFlowResourceList[resNum];
                if(!res.added) {
                    if(eila.checkAddResource(res.name, 1)) {
                        res.added = true;                    
                        eila.addResource(res.name, 1);
                        $('.addManualResource_'+resNum).css('visibility','hidden');
                    }
                    else {
                        if(res.name === FEAR) {
                            let confirmHTML = '<div>Are you sure you want to remove this <div class="resource resource_'+res.name +'"></div> ?<br/>Removing it will cost you one <div class="resource resource_heart"></div> !</div><div class="chooseone chooseConfirm"><button class="buttonYes">Yes</button><button class="popupOk">No</button></div>';
                            game.messagePopup(confirmHTML, null, null, true);
                            $('#popup').addClass('bigPromptPopup');
                            $('.buttonYes').on('click',function(){      
                                event.preventDefault();
                                res.added = true;
                                $('.addManualResource_'+resNum).css('visibility','hidden');
                                eila.removeResource(HEART, 1);
                                $('#popup').removeClass('bigPromptPopup');
                                $('.popupOk:first').trigger('click');
                                allowRemovalWhenFull();
                            });
                            $('.buttonNo').on('click',function(){    
                                event.preventDefault();
                                $('#popup').removeClass('bigPromptPopup');
                            });
                        }
                        else {
                            let confirmHTML = '<div>Are you sure you want to remove this <div class="resource resource_'+res.name +'"></div> ?</div><div class="chooseone chooseConfirm"><button class="buttonYes">Yes</button><button class="popupOk">No</button></div>';
                            game.messagePopup(confirmHTML, null, null, true);
                            $('.buttonYes').on('click',function(){      
                                event.preventDefault();
                                res.added = true;
                                $('.addManualResource_'+resNum).css('visibility','hidden');
                                $('.popupOk:first').trigger('click');
                                allowRemovalWhenFull();
                            });
                        }
                    }                    
                }
                allowRemovalWhenFull();
            });

            $('#modalPopup').fadeIn();
            allowRemovalWhenFull();
        }

        getItemPopup(trade) {
            $('#cardWrapper').addClass('unclickable');            
            $('#modalPopup').addClass('fullHeight');
            $('#modalPopup .popupContent').html('');           
            for(let itemName of trade.to) {
                let tradeType = $('<div/>').addClass('tradeType');
                if(parseInt(trade.fromAmount) !== 0) {
                    tradeType.append(trade.fromAmount+'&nbsp;');
                    tradeType.append($('<div/>').addClass('resource resource_'+trade.from));
                    tradeType.append($('<div/>').addClass('arrow'));
                }                
                tradeType.append($('<div/>').addClass('item item_'+itemName));
                tradeType.on('click',function(event){
                    event.preventDefault();
                    sound.play('click');
                    let itemTrade = new Trade(trade.from, trade.fromAmount, itemName , ITEM);
                    itemTrade.execute();

                    $('#modalPopup').fadeOut();
                    game.removeUnclick();
                    $('#modalPopup').removeClass('fullHeight');
                    game.getNextCard('getItemPopup');
                });                
                $('#modalPopup .popupContent').append(tradeType);
            }

            $('#modalPopup').fadeIn();
        }

        dayEndPopup(step) {
            $('#cardWrapper').addClass('unclickable');
            switch(step) {
                case 1:
                    $('#modalPopup .popupContent').html('<div>Day '+this.day+' has ended.<br/>1. Fulfill goals </div>');
                    $('#modalPopup .popupContent').append($('<button/>').text('Done').addClass('popupButton nextStep'));
                    $('.nextStep').on('click',function(event){
                        event.preventDefault();
                        sound.play('click');
                        game.dayEndPopup(2);
                    });
                    game.goal.enableScoring();
                break;
                case 2:                    
                    game.goal.disableScoring();
                    if(eila.resources[ENERGY] > 0) {     
                        game.goal.dayMarkers.push(ENERGY);
                        $('#modalPopup .popupContent').html('<div>2. Lose one <div class="resource resource_energy"></div></div>');
                        eila.removeResource(ENERGY, 1);
                    }
                    else {
                        game.goal.dayMarkers.push(HEART);
                        $('#modalPopup .popupContent').html('<div>2. As you lack <div class="resource resource_energy"></div><br/>Lose one <div class="resource resource_heart"></div>instead</div>');
                        eila.addResource(HEART, -1);
                    }
                    $('#modalPopup .popupContent').append($('<button/>').text('Ok').addClass('popupButton nextStep'));
                    $('.nextStep').on('click',function(event){
                        event.preventDefault();
                        sound.play('click');
                        game.dayEndPopup(3);                      
                    });
                    this.day++;
                    game.goal.updateOverlay();
                    game.checkLoseConditions();
                break;
                case 3:
                    game.dayEnd = false;
                    $('#game').removeClass('dayEnd');
                    $('#goalCard').trigger('click');
                    $('#modalPopup .popupContent').html('<div class="smallerContent">3. The future cards have been shuffled into the present.</div>');
                    $('#modalPopup .popupContent').append($('<button/>').text('Let\'s go').addClass('popupButton modalOk'));
                break;
            }
            $('#modalPopup').fadeIn();
        }

        endGame(message) {
            $('#modalPopup').hide();
            $('#popup').hide();
            $('#cardHandle').hide();
            game.dayEnd = false;
            $('#goalCard').trigger('click');
            $('#bodyFormula').off();
            $('#mindFormula').off();
            $(document).off();
            game.messagePopup(message,'Back to menu','toMenu');
            $('.toMenu').on('click', function(event) {
                event.preventDefault();
                sound.play('click');
                $('#popup').fadeOut();
                $('#game').fadeOut();
                location.reload();
            }); 
        }

        checkVictoryConditions() {
            if(!this.ended && this.goal.resourceList.findIndex(r => r.found === false) === -1) {
                this.endGame('Well done<br>You have won');
                this.addHistory('won');
                this.ended =  true;
                
                console.log(this.history);
                this.storeResult(true);
                storyBook.startStory(this.difficulty,true,true);
            }
        }

        checkLoseConditions() {
            if(!this.ended && eila.resources[HEART] < 1 || this.day > 7) {
                this.endGame('Oh no !<br>You have lost');
                this.addHistory('lost');
                this.ended =  true;
                
                console.log(this.history);
                this.storeResult(false);
                storyBook.startStory(this.difficulty,false,true);
            }
        }
        
        storeResult(won) {
            $.post('//boardwebgames.com/eila/server.php', {"data":JSON.stringify(this.history), "won":won, "diff":this.difficulty, "day":this.day});
        }

        addHistory(event) {
            this.history.push([event,new Date().toLocaleTimeString()]);
        }
        
        removeUnclick() {            
            function executeRemoval() {
                if($('.showItem:visible').length === 0 && $('#popup:visible').length === 0 && $('#modalPopup:visible').length === 0) {
                    $('#cardWrapper').removeClass('unclickable');
                }
            }
            
            executeRemoval();
            clearTimeout(game.removalTimeout);
            if($('#cardWrapper').hasClass('unclickable')) {
                game.removalTimeout = setTimeout(function(){
                    executeRemoval();
                },500);
            }
        }
    }
    
    class Tutorial {
        constructor() {
            $('#tutorial').fadeIn();
            this.infoTexts = [
                {"txt":'I am dr.owl, and I will help you learn to play this game. Click me and I will explain more.',"card":['001']},
                {"txt":'Each card has one or more options to choose from.',"card":['001']},
                {"txt":'An option with an arrow to the left means this card goes to the Past, never to return.',"card":['001']},
                {"txt":'An arrow to the right means this card goes to the Future, and will return the next day <div class="resource resource_day"></div>.',"card":['001']},
                {"txt":'Options with no arrow will go neither Past nor Future and return depending on your choices.',"card":['010','011']},
                {"txt":'Some options require an item before you can use them, for example a Toolbox <div class="item item_toolbox"></div>.',"card":['003']},
                {"txt":'Once an option has been chosen you will gain the resources or cards associated with it.',"card":['001']},
                {"txt":'Resources like <div class="resource resource_food"></div>, <div class="resource resource_coin"></div> and <div class="resource resource_star"></div> go to your Hand, in the 8 slots on the left side.',"card":['001']},
                {"txt":'Resources like <div class="resource resource_energy"></div>, <div class="resource resource_wisdom"></div> and <div class="resource resource_fear"></div> go to your Heart, in the 8 slots on the right side.',"card":['002']},
                {"txt":'You can only discard resources from your Hand when adding more resources than you can carry.',"card":['001']},
                {"txt":'You can only discard resources from your Heart when adding more resources than you can carry.',"card":['002']},
                {"txt":'Some options allow you to trade resources and can only be chosen if you have them.',"card":['004']},
                {"txt":'Trades using X allow you to trade these as many times as you like. Or just zero times.',"card":['005','010']},
                {"txt":'The trades on the big yellow and red button at the bottom can be used at any time.',"card":['004','010']},   
                {"txt":'Keep in mind that you need one <div class="resource resource_energy"></div> at the end of the day.',"card":['004']},
                {"txt":'The next day <div class="resource resource_day"></div> event happens when your deck of Present cards <div class="resource resource_present"></div> runs out.',"card":['004','007']},
                {"txt":'The <div class="resource resource_present"></div> in the top bar represents your Present deck.',"card":['004']},
                {"txt":'Besides gaining, trading and losing resources some options will give you new cards.',"card":['001']},
                {"txt":'Cards with the blue icon go to the Future <div class="resource resource_future"></div> and you will encounter them on the next day.',"card":['001','009']},
                {"txt":'Go ahead, click one of the options.',"card":['001']},
                {"txt":'Cards with the <div class="resource resource_present"></div> go to the Present and you will encounter them immediatly.',"card":['006','008']},
                {"txt":'Fear <div class="resource resource_fear"></div> is the only resource you can never manually remove, only by certain cards.',"card":['006','011']},
                {"txt":'Therefor <div class="resource resource_fear"></div> can block your Heart from gaining other resources.',"card":['006','011']},
                {"txt":'At this point you might wonder what is my goal. To see it click on the top bar.',"card":['005','007']},
                {"txt":'This will reveal the goal card for this chapter.',"card":['005','007']},
                {"txt":'Left on the goal card are the days <div class="resource resource_day"></div> you have to reach your goal (7).',"card":['005','007']},
                {"txt":'Top right shows the resources you need to gather (two <div class="resource resource_wisdom"></div>).',"card":['005','007']},
                {"txt":'Bottom right shows your <div class="resource resource_heart"></div>. In the middle you see what happens at the end of a day.',"card":['005','007']},                
                {"txt":'The first day has ended, you can now fullfil some of the resources required to win the game.',"card":['day1']},
                {"txt":'If you have any of the required <div class="resource resource_wisdom"></div>. Click on them to score them.',"card":['day1']},
                {"txt":'For the next step you need one <div class="resource resource_energy"></div>, if you do not have one but you do have two <div class="resource resource_food"></div>, use the yellow trade button at the very bottom.',"card":['day1']},
                {"txt":'End of day steps: step 1. fullfil one or two resources.',"card":['005','007','day1']},
                {"txt":'Step 2. lose one <div class="resource resource_energy"></div>, if you lack <div class="resource resource_energy"></div> you will lose a <div class="resource resource_heart"></div> instead !',"card":['005','007','day1']},
                {"txt":'Step 3. All Future cards <div class="resource resource_future"></div> are shuffled and form the new Present deck <div class="resource resource_present"></div>.',"card":['005','007','day1']},
                {"txt":'Gather all required resources to win the game. Run out of <div class="resource resource_heart"></div> or <div class="resource resource_day"></div> and you lose.',"card":['005','007','day1']}
            ];
            this.cardInfos = [];
            this.infoTextCounter = 0;            
            this.cardTutorial('001');
            
            $('#tutorial').on('click',function(event){
                event.preventDefault();
                sound.play('click');
                tutorial.nextInfo();
            });
        }
        
        cardTutorial(cardId) {
            this.cardInfos = this.infoTexts.filter(c => c.card.indexOf(cardId) !== -1);
            this.infoTextCounter = 0;
            this.nextInfo();
            $('#tutorial').fadeIn();
        }
        
        showText(text) {
            $('#tutorialText').html('\"'+text+'\"');
        }
        
        activateSpecials(specials) {
            console.log('activateSpecials',specials);
        }
        
        nextInfo() {
            if(this.infoTextCounter >= this.cardInfos.length) {
                $('#tutorial').fadeOut();
            }
            else {
                $('.tutorialSpecial').removeClass('tutorialSpecial');
                let textItem = this.cardInfos[this.infoTextCounter];
                this.showText(textItem.txt);
                if(textItem.special) {
                    this.activateSpecials(textItem.special);
                }
                this.infoTextCounter++;       
            }
        }
    }

    class Eila {
        constructor() {
            this.resources = {};
            this.resources[HEART] = game.goal.heart;
            this.resources[COIN] = 0;
            this.resources[FOOD] = 0;
            this.resources[STAR] = 0;
            this.resources[ENERGY] = 0;
            this.resources[FEAR] = 0;
            this.resources[WISDOM] = 0;

            this.body = [COIN,FOOD,STAR,BODY];
            this.mind = [FEAR,ENERGY,WISDOM,MIND];
            this.maxBody = 8;
            this.maxMind = 8;

            this.override = false;  // allow resources to be removed when they do not fit

            this.items = [];

            eila = this;            
            this.refreshResources();
            game.goal.updateOverlay();
        }

        bodySlots() {
            return this.resources[COIN] + this.resources[FOOD] + this.resources[STAR];
        }

        mindSlots() {
            return this.resources[ENERGY] + this.resources[FEAR] + this.resources[WISDOM];
        }

        resourceCheck(name) {
            if(this.body.indexOf(name) > -1) {
                return {"type":BODY, "slots": this.maxBody - this.bodySlots()};
            }
            if(this.mind.indexOf(name) > -1) {
                return {"type":MIND, "slots": this.maxMind - this.mindSlots()};
            }
        }

        checkAddResource(name, amount) {
            if(amount < 0 && name !== HEART && name !== FEAR) {
                return this.checkRemoveResource(name, -amount);
            }
            if(name === HEART) {
                return true;
            }
            let resource = this.resourceCheck(name);
            if(resource.slots >= amount || this.override) {          
                return true;
            }
            // no more room to add resource
            return null;
        }

        checkRemoveResource(name, amount) {
            if(amount < 0) {
                return this.checkAddResource(name, -amount);
            }
            let resourceCount = this.resources[name];
            if(amount <= resourceCount) {
                return true;
            }
            else {
                // message player, has not enough of resource
                game.messagePopup('<span>You do not have enough <div class="resource resource_'+name+'"></div> for this action.</span>','Ok');
                return false;
            }
            return null;
        }

        addResource(name, amount) {
            if(name === HEART) {                
                this.resources[name] += amount;
                this.refreshResources();   
                game.checkLoseConditions();
            }
            else {
                let resource = this.resourceCheck(name);
                this.resources[name] += Math.min(amount,resource.slots);   
                if(this.resources[name] < 0) {
                    this.resources[name] = 0;
                }
                this.refreshResources();   
            }            
        }

        removeResource(name, amount) {
            this.resources[name] -= amount;
            if(this.resources[name] < 0) {
                this.resources[name] = 0;
            }
            this.refreshResources();
            if(name === HEART) {
                game.goal.updateOverlay();
                game.checkLoseConditions();
            }
        }

        hasItem(name) {
            return this.items.findIndex(e => e.name === name) !== -1;            
        }  

        refreshResources() {
            this.showResources(BODY);
            this.showResources(MIND);
            var hearts = '';
            for(let h=0; h < this.resources[HEART];  h++) {
                hearts += '<div class="resource resource_heart"></div>';
            }
            $('#health').html(hearts);
            var inventory = [];
            for(let item of this.items) {
                inventory.push($('<div/>').addClass('item item_'+item.name).on('click',function(event){
                    event.preventDefault();
                    event.stopPropagation();
                    eila.showItem(item.img);
                }));
            }
            $('#inventory').html('').append(inventory);
        }

        showResources(resourceGroup) {
            var resourceList = [];
            for(let resource of this[resourceGroup]) {
                if(this.resources[resource] > 0) {
                    for(let amount = 0; amount < this.resources[resource]; amount++) {
                        resourceList.push(resource);
                    }
                }
            }

            $('#'+resourceGroup+'Inventory .inventorySlot').html('').removeClass('hasResource');
            for(let [index,res] of resourceList.entries()) {
                $('#'+resourceGroup+'Inventory .inventorySlot.slot'+(index+1)).html('<div class="resource resource_'+res+'" res="'+res+'"></div>').addClass('hasResource');
            }
        }

        manuallyAddResources(resources) {                    
            if(resources.length > 0) {
                let overflow = [];
                for(let res of resources) {
                    if(res.a > 0) {
                        overflow.push(res);
                    }
                    else {
                        this.addResource(res.n,res.a);
                    }
                }
                game.manualAddPopup(overflow);
                return true;
            }
            return false;
        }

        showItem(itemImg) {
            if($('.showItem').attr('src') === itemImg ) {
                $('.showItem').remove();
                game.removeUnclick();
            }
            else {
                $('#cardWrapper').addClass('unclickable');
                $('.showItem').remove();
                var myItem = $('<img/>').attr('src',itemImg).addClass('showItem').on('click',function(event){
                    event.preventDefault();
                    sound.play('click');
                   $(this).remove(); 
                   game.removeUnclick();
                });

                $('#wrapper').append(myItem);
            }
        }
    }

    class Deck {
        constructor(name) {
            this.name = name;      
            this.cards = [];
            decks[name] = this;
        }

        addCard(card) {
            if(card) {
                this.cards.push(card);
                game.updateTopBar();
            }
        }

        addFirstCard(card) {
            if(card) {
                this.cards.unshift(card);
                game.updateTopBar();
            }
        }

        removeCard(cardId) {
            let cardIndex = this.cards.findIndex(c => c.id === cardId);
            if(cardIndex !== -1) {
                this.cards.splice(cardIndex,1);
                game.updateTopBar();
            }
        }

        getCard(cardId) {
            let card = this.cards.find(c => c.id === cardId);
            if(card) {      
                this.removeCard(card.id);
                return card;
            }
            return null;
        }

        getFirstCard() {
            if(this.cards.length > 0) {
                let card = this.cards[0];
                this.removeCard(card.id);
                return card;
            }
            return null;
        }

        shuffle() {
            var j, x, i;
            for (i = this.cards.length; i; i -= 1) {
                j = Math.floor(Math.random() * i);
                x = this.cards[i - 1];
                this.cards[i - 1] = this.cards[j];
                this.cards[j] = x;
            }
        };

        empty() {
            this.cards = [];            
        }
    }

    class Card {
        constructor(id, choices, startdeck, instant) {
            this.id = id;
            this.img = 'img/'+id+'.jpg';
            this.choices = choices;
            this.instant = instant;
            if(startdeck) {
                decks[PRESENT].addCard(this);
            } 
            else {
                decks[POSSIBLE].addCard(this);
            }
            for(let choice of choices) {
                choice.card = this;
            }
        }

        show() {
            game.currentCard = this;
            $('#cardFront').css('background-image', 'url(' + this.img + ')');
            $('.cardOption').remove();
            this.addOptions();
        }

        addOptions() {
            let optionCount = this.choices.length;
            let optionsHTML = [];            
            for(let [index,option] of this.choices.entries()) {
                optionsHTML.push($('<div/>').addClass('cardOption cardOption_'+optionCount+'_'+index).attr('opt',index));
            }
            $('#cardFront').append(optionsHTML);
        }
    }

    class Choice {
        constructor(order, toDeck, addCards, itemRequired, resources, trades, getItem) {
            this.card;
            this.order = order;
            this.toDeck = toDeck;
            this.addCards = addCards?addCards:null;
            this.resources = resources?resources:[];
            this.trades = trades;
            this.itemRequired = itemRequired?itemRequired:null;
            this.getItem = getItem?getItem:null;
        }

        execute() {
            game.resolveDirection = this.toDeck;
            var passedChecks = true;
            if(this.itemRequired && !eila.hasItem(this.itemRequired)) {
                // notify player he lacks the item required
                game.messagePopup('<span>You lack the <div class="item item_'+this.itemRequired+'"></div> required for this action.</span>','Ok');
                passedChecks = false;
            }

            if(passedChecks && this.getItem && this.getItem instanceof Trade) {
                let resourceCheck = eila.checkRemoveResource(this.getItem.from, this.getItem.fromAmount);
                if(!resourceCheck) {
                    game.messagePopup('<span>You lack the <span class="resource resource_'+this.getItem.from+'"></span> required for this trade.</span>','Ok');
                    passedChecks = false;
                }
            }
            
            if(passedChecks && this.resources) {
                for(let resource of this.resources) {
                    if(resource.a < 0) {
                        passedChecks = eila.checkAddResource(resource.n,resource.a);
                    }
                }
            }

            if(passedChecks) {
                var canContinue = true;
                if(this.trades) {
                    var tradesOk = true;
                    for(let trade of this.trades) {
                        if(trade.check() === false) {
                            tradesOk = false; 
                            canContinue = false;
                        }
                        if(trade.check() === null) {
                            tradesOk = null; 
                            canContinue = false;
                        }
                    }
                    if(tradesOk) {
                        // resolve the full trade
                        for(let trade of this.trades) {
                            trade.execute();
                        }
                    }
                    else {
                        if(tradesOk === null) {
                            // only take do the left side of the trade at first
                            let resourceToManuallyAdd = [];
                            for(let trade of this.trades) {
                                trade.execute(true);
                                resourceToManuallyAdd.push({"n":trade.to,"a":parseInt(trade.toAmount)});
                            }
                            // only take do the left side of the trade at first
                            eila.manuallyAddResources(resourceToManuallyAdd);
                        }
                        else {
                            return;
                        }
                    }
                }

                if(this.resources) {
                    var resourcesOk = true;
                    var totalPerType = {'body':0,'mind':0};
                    for(let resource of this.resources) {
                        if(resource.n !== HEART) {
                            totalPerType[eila.resourceCheck(resource.n).type] += resource.a;
                            let resourceCheck = eila.checkAddResource(resource.n, resource.a);
                            if(!resourceCheck) {
                                if(resourcesOk !== null) {
                                    resourcesOk = resourceCheck;
                                }
                                canContinue = false;
                            }
                        }
                    }

                    if( !(eila.resourceCheck(BODY).slots >= totalPerType[BODY] && eila.resourceCheck(MIND).slots >= totalPerType[MIND])) {
                        resourcesOk = null;
                        canContinue = false;
                    }

                    if(resourcesOk === true) {
                        for(let resource of this.resources) {
                            eila.addResource(resource.n, resource.a);
                        }
                    }
                    else {
                        if(resourcesOk === null) {
                            if(!eila.manuallyAddResources(this.resources)) {
                                for(let resource of this.resources) {
                                    eila.addResource(resource.n, resource.a);
                                }
                                canContinue = true;
                            }
                        }
                    }
                }

                if(this.addCards) {
                    for(let addCard of this.addCards) {
                        let targetDeck = FUTURE;
                        let targetCard = decks[POSSIBLE].getCard(addCard);
                        if(targetCard.instant) {
                            targetDeck = PRESENT;
                        }
                        decks[targetDeck].addFirstCard(targetCard);
                    }
                }

                if(canContinue) {
                    if(this.getItem) {
                        if(this.getItem instanceof Trade) {
                            game.getItemPopup(this.getItem);
                            canContinue = false;
                        }
                        else {
                            items.getItem(this.getItem);
                        }
                    }
                }     

                if(canContinue) {
                    game.getNextCard('executeChoice');
                }
                game.addHistory('card '+this.card.id+' -> option '+this.order);
            }
        }                
    }

    class Trade {
        constructor(from, fromAmount, to, toAmount) {
            this.from = from;
            this.fromAmount = fromAmount;
            this.to = to;
            this.toAmount = toAmount;
        }

        check(shopTrade) {
            let fromAmount;
            let toAmount;
            if(this.fromAmount === 'X' || this.fromAmount === '-X') {
                // ask player for the amount to trade
                game.tradePopup(this);
            }
            else {
                fromAmount = parseInt(this.fromAmount);
                toAmount = parseInt(this.toAmount);
                            
                if(shopTrade) {
                    let fromRes = eila.resourceCheck(this.from);
                    let toRes = eila.resourceCheck(this.to);
                    if(fromRes.type === toRes.type) {
                        if(eila.checkRemoveResource(this.from, fromAmount)) {
                            return true;
                        } 
                    }
                    else {
                        if(eila.checkRemoveResource(this.from, fromAmount) && eila.checkAddResource(this.to, toAmount)) {
                            return true;
                        }
                        if(eila.checkRemoveResource(this.from, fromAmount) && !eila.checkAddResource(this.to, toAmount)) {
                            return null;
                        }
                    }
                }
                else {
                    if(eila.checkRemoveResource(this.from, fromAmount) && eila.checkAddResource(this.to, toAmount)) {
                        return true;
                    }

                    if(eila.checkRemoveResource(this.from, fromAmount) && !eila.checkAddResource(this.to, toAmount)) {
                        return null;
                    }
                }

            }
            return false;
        }

        execute(removeOnly) {
            let fromAmount = parseInt(this.fromAmount);
            let toAmount;
            if(items.isItem(this.to)) {
                items.getItem(this.to);
                toAmount = ITEM;
                eila.removeResource(this.from, fromAmount);
            }
            else {     
                eila.removeResource(this.from, fromAmount);  
                if(!removeOnly) {
                    toAmount = parseInt(this.toAmount);                               
                    eila.addResource(this.to, toAmount);
                    game.addHistory('trade '+fromAmount+' '+this.from+' for '+toAmount+' '+this.to);
                }
                else {
                    game.addHistory('trade '+fromAmount+' '+this.from+' in manual trade');
                }
            }
            
        }
    }

    class Item {
        constructor(name) {
            this.name = name;
            this.img = 'img/'+name+'.jpg';
        }
    }

    class Items {
        constructor() {        
            this.itemList = {};
            items = this;
        }

        addItem(item) {
            this.itemList[item.name] = item;
        }

        isItem(name) {
            return this.itemList[name];
        }

        getItem(name) {
            let thisItem = this.itemList[name];
            if(thisItem) {
                eila.items.push(thisItem);
                delete this.itemList[name];
                eila.refreshResources();
            }
        }
    }

    class Goal {
        constructor(name, resources, heart, img, days) {  
            this.name = name;
            this.heart = heart;
            this.days = days;
            this.img = 'img/goal'+img+'.jpg';
            this.resourceList = [];
            this.required = {};
            this.scoredToday = 0;
            this.dayMarkers = [];

            for(let res of resources) {
                if(this.required[res]) {
                    this.required[res]++;
                }
                else {
                    this.required[res] = 1;
                }
                this.resourceList.push({"resource":res, "found":false});
            }
            game.goal = this;
            $('#goalCard').css('background-image', 'url(' + this.img + ')');
            this.createOverlay();            
        }

        createOverlay() {
            $('#goalCard').addClass('goalType_'+this.name);
            var slots = [];
            // required resources            
            for(let [index,res] of this.resourceList.entries()) {
                slots.push($('<div/>').addClass('goalSlot goalSlot_'+index));
            }

            // days
            for(let d=1; d <= 7; d++) {
                slots.push($('<div/>').addClass('daySlot daySlot_'+d));
            }
            // hearts 
            for(let h=1; h <= this.heart; h++) {
                slots.push($('<div/>').addClass('heartSlot heartSlot_'+h));
            }

            $('#goalCard').append(slots);           
        }

        updateOverlay() {        
            let livesLost = this.heart - eila.resources[HEART];
            if(livesLost > 0) {
                for(let h=1; h <= livesLost; h++) {
                    if($('.heartSlot'+h+' .marker').length < 1) {
                        $('.heartSlot_'+h).html('<div id="heartMarker" class="marker marker_heart"></div>');
                    }
                }
            }
            for(let [index,res] of this.resourceList.entries()) {
                if(res.found) {
                    $('.goalSlot_'+index).html('<div id="goalMarker" class="marker marker_goal"></div>');
                }
            }
            for(let [index, marker] of this.dayMarkers.entries()) {
                $('.daySlot.daySlot_'+(index+1)).html('<div class="resource resource_'+marker+'"></div>');
            }
        }

        scoreGoalResource(resource) {
            if(eila.resources[resource] > 0 && this.required[resource] > 0 && this.scoredToday < 2) {
                this.required[resource]--;
                eila.removeResource(resource,1);
                let resourceListIndex = this.resourceList.findIndex(r => r.resource === resource && r.found === false);
                if(resourceListIndex !== -1) {
                    let resourceListItem = this.resourceList[resourceListIndex];
                    resourceListItem.found = true;
                }
                this.disableScoring();
                this.updateOverlay();                                
                this.scoredToday++;
                game.addHistory('score '+resource);
                game.checkVictoryConditions();
                if(!game.gameEnded) {
                    if(this.scoredToday === 2) {                
                        game.dayEndPopup(2);
                    }
                    else {
                        this.enableScoring();
                    }
                }
            }            
        }

        enableScoring() {
            for(let [index,slot] of this.resourceList.entries()) {
                if(!slot.found) {
                    $('.goalSlot.goalSlot_'+index).addClass('scoreMe').on('click',function(){
                        event.preventDefault();
                        game.goal.scoreGoalResource(slot.resource);                        
                    });
                    $('.inventoryBar .resource.resource_'+slot.resource).addClass('scoreThis');
                }
            }    
        }

        disableScoring() {
            $('.scoreMe').removeClass('scoreThis');
            $('.scoreMe').removeClass('scoreMe').off();
        }
    }

    var init = (difficulty) => {                
        new Deck(PAST);
        new Deck(PRESENT);
        new Deck(FUTURE);
        new Deck(POSSIBLE);

        new Items();
        items.addItem(new Item('harmonica'));
        items.addItem(new Item('magnifier'));
        items.addItem(new Item('pendant'));
        items.addItem(new Item('rope'));
        items.addItem(new Item('toolbox'));  
        storyBook = new StoryBook();

        if(difficulty === 'tutorial') {
            let startCard = new Card('001',[
                new Choice(1,FUTURE,null,null,[{n:FOOD,a:1}]),
                new Choice(2,PAST,['006'])], true);
            new Card('002',[
                new Choice(1,PAST,null,null,[{n:ENERGY,a:1}]),
                new Choice(2,PAST,null,null,[{n:WISDOM,a:1}])], true);
            new Card('003',[
                new Choice(1,FUTURE,null,null,[{n:FOOD,a:1}]),
                new Choice(2,FUTURE,null,'toolbox',[{n:FOOD,a:3}])], true);
            new Card('004',[
                new Choice(1,FUTURE,null,null,[{n:COIN,a:1}]),
                new Choice(2,FUTURE,null,null,null,[new Trade(ENERGY,1,COIN,3)])], true);
            new Card('005',[
                new Choice(1,FUTURE,null,null,null,[new Trade(COIN,'X',FOOD,'X')]),
                new Choice(2,FUTURE,null,null,null,[new Trade(FOOD,'X',COIN,'X')])], true);
            new Card('006',[
                new Choice(1,PAST,['009'],null,[{n:HEART,a:-1},{n:FEAR,a:2}])]);
            new Card('007',[
                new Choice(1,FUTURE,null,null,null,[new Trade(COIN,2,WISDOM,1)]),
                new Choice(2,FUTURE,null)]);
            new Card('008',[
                new Choice(1,FUTURE,['010']),
                new Choice(2,FUTURE,['011'])]);
            new Card('009',[
                new Choice(1,PAST,['007','008'], null,[{n:FOOD,a:3}])],false, true);
            new Card('010',[
                new Choice(1,POSSIBLE,null, null,null, [new Trade(FOOD,'X',ENERGY,'X')])],false, true);
            new Card('011',[
                new Choice(1,POSSIBLE,null, null,[{n:FEAR,a:-1}])],false, true);
            new Goal(difficulty, [WISDOM,WISDOM], 3, 0, 7);
            game.setStartCard(startCard);
            intro.preloadChapter0();
        }
        else {
            let startCard = new Card('101',[
                new Choice(1,PAST,['110'],null,[{n:ENERGY,a:3}]),
                new Choice(1,PAST,['110'],null,[{n:WISDOM,a:2}])], true);
            new Card('102',[
                new Choice(1,FUTURE,null,null,[{n:FOOD,a:1}]),
                new Choice(2,FUTURE,null,'toolbox',[{n:FOOD,a:3}])], true);
            new Card('103',[
                new Choice(1,FUTURE,null,null,[{n:COIN,a:1}]),
                new Choice(2,FUTURE,null,null,null,[new Trade(ENERGY,1,COIN,3)])], true);
            new Card('104',[
                new Choice(1,FUTURE,null,null,null,[new Trade(COIN,'X',FOOD,'X')]),
                new Choice(2,FUTURE,null,null,null,[new Trade(FOOD,'X',COIN,'X')]),
                new Choice(3,PAST,['111'],null,null,null,new Trade(COIN,'3',['rope','toolbox','magnifier'], ITEM))], true);
            new Card('105',[
                new Choice(1,FUTURE,null,null,[{n:FEAR,a:2}]),
                new Choice(2,PAST,null,null,[{n:WISDOM,a:-1}])], true);
            new Card('106',[
                new Choice(1,PAST,['113'],null,[{n:FOOD,a:3}]),
                new Choice(2,PAST,['113'],null,[{n:WISDOM,a:1}])], true);
            new Card('107',[
                new Choice(1,FUTURE,null),
                new Choice(2,PAST,['124'],null,[{n:FOOD,a:-1}])], true);
            new Card('108',[
                new Choice(1,FUTURE,null,null,[{n:FOOD,a:1},{n:ENERGY,a:1}]),
                new Choice(2,FUTURE,null,null,[{n:WISDOM,a:1}]),
                new Choice(3,FUTURE,null,null,[{n:FEAR,a:-2}])], true);
            new Card('109',[
                new Choice(1,FUTURE,null,null,null,[new Trade(FOOD,'X',ENERGY,'X')])], true);
            new Card('110',[
                new Choice(1,PAST,['115','116'],null,[{n:ENERGY,a:-1}]), 
                new Choice(2,PAST,['115','116'],null,[{n:HEART,a:-1}]) ],false);
            new Card('111',[
                new Choice(1,FUTURE,null,null,null,[new Trade(COIN,'X',FOOD,'X')]),
                new Choice(2,FUTURE,null,null,null,[new Trade(FOOD,'X',COIN,'X')])], false);
            new Card('112',[
                new Choice(1,PAST,['H01']),
                new Choice(2,PAST,['H02'])], false);
            new Card('113',[
                new Choice(1,PAST,['H02']),
                new Choice(2,PAST,['117','H03'])], false);
            new Card('114',[
                new Choice(1,FUTURE,null),
                new Choice(2,PAST,['125'],null,[{n:WISDOM,a:-2}]),
                new Choice(3,PAST,['125'],'toolbox',[{n:ENERGY,a:-1}])], false);
            new Card('115',[
                new Choice(1,PAST,['119','120'],null,[{n:ENERGY,a:-1}]),
                new Choice(2,PAST,['119','120'],null,[{n:HEART,a:-1}])], false);
            new Card('116',[
                new Choice(1,FUTURE,['126'],null,[{n:ENERGY,a:-3}]),
                new Choice(2,FUTURE,['126'],'rope',[{n:ENERGY,a:-1}]),
                new Choice(3,FUTURE,['126'],'magnifier'),
                new Choice(4,FUTURE,null)], false);
            new Card('117',[
                new Choice(1,PAST,null,null,[{n:FOOD,a:2},{n:COIN,a:2}])], false);
            new Card('118',[
                new Choice(1,PAST,['127'],null,[{n:ENERGY,a:-2}]), 
                new Choice(2,PAST,['127'],'rope',[{n:ENERGY,a:-1}]), 
                new Choice(3,FUTURE,null)], false);
            new Card('119',[
                new Choice(1,PAST,['122'],null,[{n:ENERGY,a:-1}]), 
                new Choice(2,PAST,['122'],null,[{n:HEART,a:-1}])], false);
            new Card('120',[
                new Choice(1,FUTURE,null,null,[{n:COIN,a:-1},{n:FEAR,a:1}]), 
                new Choice(2,FUTURE,null,null,[{n:FEAR,a:2}])], false);
            new Card('121',[
                new Choice(1,PAST,null,null,[{n:FEAR,a:-3},{n:STAR,a:1}], null, 'pendant')], false);
            new Card('122',[
                new Choice(1,PAST,['123','128'],null,[{n:FEAR,a:1}])], false);
            new Card('123',[
                new Choice(1,FUTURE,null,null,[{n:ENERGY,a:-2}]), 
                new Choice(2,FUTURE,null,null,[{n:HEART,a:-1}])], false);
            new Card('124',[
                new Choice(1,PAST,['114'])], false, true);
            new Card('125',[
                new Choice(1,PAST,['118'])], false, true);
            new Card('126',[
                new Choice(1,POSSIBLE,null, null, null, [new Trade(COIN,2,WISDOM,2)]), 
                new Choice(2,POSSIBLE,null, null, [{n:WISDOM,a:1}])], false, true);
            new Card('127',[
                new Choice(1,PAST,['121'], null, [{n:FOOD,a:2}])], false, true);
            new Card('128',[
                new Choice(1,FUTURE,null, null, [{n:ENERGY,a:-1}, {n:FEAR,a:1}]), 
                new Choice(2,FUTURE,null, null, [{n:HEART,a:-1}, {n:FEAR,a:1}])], false, true);
            new Card('H01',[
                new Choice(1,FUTURE,null,null,[{n:FEAR,a:-1}, {n:ENERGY,a:1}])], false, true);
            new Card('H02',[
                new Choice(1,FUTURE,null,null,[{n:FOOD,a:3}]), 
                new Choice(2,FUTURE,null,'toolbox',[{n:WISDOM,a:-1}, {n:FOOD,a:7}])], false, true);
            new Card('H03',[
                new Choice(1,FUTURE,null,null,[{n:FEAR,a:-2}]), 
                new Choice(2,FUTURE,null,'harmonica',[{n:FEAR,a:-2}, {n:COIN,a:1}])], false, true);

            if(difficulty === 'advanced') {
                new Goal(difficulty,[FOOD,FOOD,FOOD,FOOD,COIN,COIN,COIN,STAR,STAR], 2, '1b', 7);
            }
            else {
                new Goal(difficulty,[FOOD,FOOD,FOOD,COIN,COIN,STAR], 4, '1a', 7);
            }
            game.setStartCard(startCard);
            intro.preloadChapter1();
        }
        new Eila;      
        
        
        //START TEMPORARY CHEAT STUFF
            let cheatDiv1 = $('<div/>').addClass('cheatdiv').attr('id','cheatAdd');
            let cheatDiv2 = $('<div/>').addClass('cheatdiv').attr('id','cheatSub');
            $('#cardWrapper').append(cheatDiv1);
            $('#cardWrapper').append(cheatDiv2);
            game.cheatAdd = 0;
            game.cheatSub = 0;
            $('.cheatdiv').on('click',function(event){
                event.preventDefault();
                event.stopPropagation();
                if($(this).attr('id') === 'cheatAdd') {
                    game.cheatAdd++;
                    if(game.cheatAdd >= 10) {
                        game.cheatAdd = 0;
                        eila.resources[COIN] = 2;
                        eila.resources[FOOD] = 4;
                        eila.resources[STAR] = 2;
                        eila.resources[ENERGY] = 4;
                        eila.resources[FEAR] = 0;
                        eila.resources[WISDOM] = 4;
                        eila.refreshResources();
                    }
                }
                if($(this).attr('id') === 'cheatSub') {
                    game.cheatSub++;
                    if(game.cheatSub >= 10) {
                        game.cheatSub = 0;
                        eila.resources[COIN] = 0;
                        eila.resources[FOOD] = 0;
                        eila.resources[STAR] = 0;
                        eila.resources[ENERGY] = 0;
                        eila.resources[FEAR] = 0;
                        eila.resources[WISDOM] = 0;
                        eila.refreshResources();
                    }
                }
            });
        //END TEMPORARY CHEAT STUFF
    };

    new Intro();  
});
